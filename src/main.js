import Modal from './components/modal.html';
import {Store} from 'svelte/store';

class HiveID {
  constructor(options) {
    if (!options.api_id) {
      throw new Error('You need add block api_id');
    }

    if (!options.api_id.partner_id) {
      throw new Error('You need add api_id.app_id');
    }

    if (!options.api_id.app_id) {
      throw new Error('You need add api_id.app_id');
    }

    this.target = options.target;
    this.apiId = options.api_id;
    this.app = null;
    this.callback = options.callback;

    this.destroy = this.destroy.bind(this);
    this.init = this.init.bind(this);
  }

  destroy() {
    this.app && this.app.destroy();
  }

  init() {
    this.app = new Modal({
      target: this.target || document.body,
      store: new Store({
        apiId: this.apiId,
        closeModal: this.destroy,
        callback: this.callback
      }),
    });
  }
}

exports.default = HiveID;