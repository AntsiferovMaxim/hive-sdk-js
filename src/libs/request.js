const url = 'https://api.staging.hive.id/sessions';

export function createSession(body, callback) {
    const contentType = ['Content-Type', 'application/json'];

    request({
        url,
        headers: [contentType],
        method: 'POST',
        body
    }, callback);
}

export function pollingRequest(sessionId, onComplete, onExpire, onScanned) {
    const intervalId = setInterval(function () {
        request({
            url: url + '/' + sessionId
        }, function (res) {
            if (res.metadata.status === 404) {
                clearInterval(intervalId);
                onExpire();
            }

            if (res.metadata.status === 202) {
                onScanned()
            }

            if (res.metadata.status === 200) {
                onComplete(res.data);
            }
        });
    }, 2000);

    return intervalId;
}

function request(options, callback) {
    const url = options.url;
    const headers = options.headers ? options.headers : [];
    const method = options.method ? options.method : 'GET';
    const body = options.body;

    if (!options) {
        throw new Error({msg: 'Empty options'})
    }

    const xhr = new XMLHttpRequest();
    xhr.open(method, url, true);

    headers.forEach(function (item) {
        xhr.setRequestHeader(item[0], item[1]);
    });

    xhr.onreadystatechange = function () {
        if (this.readyState !== 4) return;

        const data = this.response ? JSON.parse(this.response) : {};

        const metadata = {
            status: this.status,
            statusText: this.statusText,
        };

        callback({metadata: metadata, data: data});
    };

    xhr.onerror = function () {
        throw new Error(this);
    };

    body ? xhr.send(JSON.stringify(body)) : xhr.send();
}
