import svelte from 'rollup-plugin-svelte';
import resolve from 'rollup-plugin-node-resolve';
import commonjs from 'rollup-plugin-commonjs';
import buble from 'rollup-plugin-buble';
import { terser } from 'rollup-plugin-terser';

const production = !process.env.ROLLUP_WATCH;
const distPath = production ? 'dist/' : 'test/';

export default {
	input: 'src/main.js',
	output: [
    {
      sourcemap: true,
      format: 'esm',
      file: distPath + 'hiveid-sdk.esm.js'
    },
    {
      sourcemap: true,
      format: 'cjs',
			name: 'HiveID',
      file: distPath + 'hiveid-sdk.cjs.js'
    }
	],
	plugins: [
		svelte({
			// opt in to v3 behaviour today
			skipIntroByDefault: true,
			nestedTransitions: true,

			// enable run-time checks when not in production
			dev: !production,
			// we'll extract any component CSS out into
			// a separate file — better for performance
		}),
		// If you have external dependencies installed from
		// npm, you'll most likely need these plugins. In
		// some cases you'll need additional configuration —
		// consult the documentation for details:
		// https://github.com/rollup/rollup-plugin-commonjs
		resolve(),
		commonjs(),
    buble(),

		// If we're building for production (npm run build
		// instead of npm run dev), minify
		production && terser()
	]
};
