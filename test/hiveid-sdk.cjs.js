'use strict';

function noop() {}

function assign(tar, src) {
	for (var k in src) { tar[k] = src[k]; }
	return tar;
}

function assignTrue(tar, src) {
	for (var k in src) { tar[k] = 1; }
	return tar;
}

function addLoc(element, file, line, column, char) {
	element.__svelte_meta = {
		loc: { file: file, line: line, column: column, char: char }
	};
}

function run(fn) {
	fn();
}

function append(target, node) {
	target.appendChild(node);
}

function insert(target, node, anchor) {
	target.insertBefore(node, anchor);
}

function detachNode(node) {
	node.parentNode.removeChild(node);
}

function createElement(name) {
	return document.createElement(name);
}

function createText(data) {
	return document.createTextNode(data);
}

function addListener(node, event, handler, options) {
	node.addEventListener(event, handler, options);
}

function removeListener(node, event, handler, options) {
	node.removeEventListener(event, handler, options);
}

function blankObject() {
	return Object.create(null);
}

function destroy(detach) {
	this.destroy = noop;
	this.fire('destroy');
	this.set = noop;

	this._fragment.d(detach !== false);
	this._fragment = null;
	this._state = {};
}

function destroyDev(detach) {
	destroy.call(this, detach);
	this.destroy = function() {
		console.warn('Component was already destroyed');
	};
}

function _differs(a, b) {
	return a != a ? b == b : a !== b || ((a && typeof a === 'object') || typeof a === 'function');
}

function _differsImmutable(a, b) {
	return a != a ? b == b : a !== b;
}

function fire(eventName, data) {
	var handlers =
		eventName in this._handlers && this._handlers[eventName].slice();
	if (!handlers) { return; }

	for (var i = 0; i < handlers.length; i += 1) {
		var handler = handlers[i];

		if (!handler.__calling) {
			try {
				handler.__calling = true;
				handler.call(this, data);
			} finally {
				handler.__calling = false;
			}
		}
	}
}

function flush(component) {
	component._lock = true;
	callAll(component._beforecreate);
	callAll(component._oncreate);
	callAll(component._aftercreate);
	component._lock = false;
}

function get() {
	return this._state;
}

function init(component, options) {
	component._handlers = blankObject();
	component._slots = blankObject();
	component._bind = options._bind;
	component._staged = {};

	component.options = options;
	component.root = options.root || component;
	component.store = options.store || component.root.store;

	if (!options.root) {
		component._beforecreate = [];
		component._oncreate = [];
		component._aftercreate = [];
	}
}

function on(eventName, handler) {
	var handlers = this._handlers[eventName] || (this._handlers[eventName] = []);
	handlers.push(handler);

	return {
		cancel: function() {
			var index = handlers.indexOf(handler);
			if (~index) { handlers.splice(index, 1); }
		}
	};
}

function set(newState) {
	this._set(assign({}, newState));
	if (this.root._lock) { return; }
	flush(this.root);
}

function _set(newState) {
	var oldState = this._state,
		changed = {},
		dirty = false;

	newState = assign(this._staged, newState);
	this._staged = {};

	for (var key in newState) {
		if (this._differs(newState[key], oldState[key])) { changed[key] = dirty = true; }
	}
	if (!dirty) { return; }

	this._state = assign(assign({}, oldState), newState);
	this._recompute(changed, this._state);
	if (this._bind) { this._bind(changed, this._state); }

	if (this._fragment) {
		this.fire("state", { changed: changed, current: this._state, previous: oldState });
		this._fragment.p(changed, this._state);
		this.fire("update", { changed: changed, current: this._state, previous: oldState });
	}
}

function _stage(newState) {
	assign(this._staged, newState);
}

function setDev(newState) {
	if (typeof newState !== 'object') {
		throw new Error(
			this._debugName + '.set was called without an object of data key-values to update.'
		);
	}

	this._checkReadOnly(newState);
	set.call(this, newState);
}

function callAll(fns) {
	while (fns && fns.length) { fns.shift()(); }
}

function _mount(target, anchor) {
	this._fragment[this._fragment.i ? 'i' : 'm'](target, anchor || null);
}

var protoDev = {
	destroy: destroyDev,
	get: get,
	fire: fire,
	on: on,
	set: setDev,
	_recompute: noop,
	_set: _set,
	_stage: _stage,
	_mount: _mount,
	_differs: _differs
};

function setDeep(keypath, value) {
    if (keypath === undefined) {
        return;
    }
    var keys = keypath.replace(/\[(\d+)\]/g, '.$1').split('.');
    var lastKey = keys.pop();
    // If not a nested keypath
    if (keys[0] === undefined) {
        var data_1 = {};
        data_1[lastKey] = value;
        this.set(data_1);
        return;
    }
    var object = this.get()[keys[0]];
    for (var i = 1; i < keys.length; i++) {
        object = object[keys[i]];
    }
    object[lastKey] = value;
    var data = {};
    data[keys[0]] = this.get()[keys[0]];
    this.set(data);
}

var url = 'https://api.staging.hive.id/sessions';

function createSession(body, callback) {
    var contentType = ['Content-Type', 'application/json'];

    request({
        url: url,
        headers: [contentType],
        method: 'POST',
        body: body
    }, callback);
}

function pollingRequest(sessionId, onComplete, onExpire, onScanned) {
    var intervalId = setInterval(function () {
        request({
            url: url + '/' + sessionId
        }, function (res) {
            if (res.metadata.status === 404) {
                clearInterval(intervalId);
                onExpire();
            }

            if (res.metadata.status === 202) {
                onScanned();
            }

            if (res.metadata.status === 200) {
                onComplete(res.data);
            }
        });
    }, 2000);

    return intervalId;
}

function request(options, callback) {
    var url = options.url;
    var headers = options.headers ? options.headers : [];
    var method = options.method ? options.method : 'GET';
    var body = options.body;

    if (!options) {
        throw new Error({msg: 'Empty options'})
    }

    var xhr = new XMLHttpRequest();
    xhr.open(method, url, true);

    headers.forEach(function (item) {
        xhr.setRequestHeader(item[0], item[1]);
    });

    xhr.onreadystatechange = function () {
        if (this.readyState !== 4) { return; }

        var data = this.response ? JSON.parse(this.response) : {};

        var metadata = {
            status: this.status,
            statusText: this.statusText,
        };

        callback({metadata: metadata, data: data});
    };

    xhr.onerror = function () {
        throw new Error(this);
    };

    body ? xhr.send(JSON.stringify(body)) : xhr.send();
}

/* src/components/auth-spinner.html generated by Svelte v2.16.1 */

var file = "src/components/auth-spinner.html";

function add_css() {
	var style = createElement("style");
	style.id = 'svelte-1k7b7mr-style';
	style.textContent = "@keyframes svelte-1k7b7mr-Spin{from{transform:rotate(0deg)}to{transform:rotate(360deg)}}.spin-element.svelte-1k7b7mr{animation:svelte-1k7b7mr-Spin 1.5s infinite ease-in-out}.spinner-container.svelte-1k7b7mr{display:flex;align-items:center;justify-content:center;height:100%}\n/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXV0aC1zcGlubmVyLmh0bWwiLCJzb3VyY2VzIjpbImF1dGgtc3Bpbm5lci5odG1sIl0sInNvdXJjZXNDb250ZW50IjpbIjxkaXYgY2xhc3M9XCJzcGlubmVyLWNvbnRhaW5lclwiPlxuICAgIDxpbWcgY2xhc3M9XCJzcGluLWVsZW1lbnRcIiBzcmM9XCJkYXRhOmltYWdlL3N2Zyt4bWw7YmFzZTY0LFBITjJaeUJqYkdGemN6MGljM0JwYmkxbGJHVnRaVzUwSWlCM2FXUjBhRDBpT1RSd2VDSWdhR1ZwWjJoMFBTSTVOSEI0SWlCMmFXVjNRbTk0UFNJd0lEQWdPVFFnT1RRaUlIWmxjbk5wYjI0OUlqRXVNU0lLSUNBZ0lDQjRiV3h1Y3owaWFIUjBjRG92TDNkM2R5NTNNeTV2Y21jdk1qQXdNQzl6ZG1jaVBnb2dJQ0FnUEdSbFpuTStDaUFnSUNBZ0lDQWdQR3hwYm1WaGNrZHlZV1JwWlc1MElIZ3hQU0k0T0M0Mk9UVTVNelVsSWlCNU1UMGlNVGt1T1RFNE1qUXdNeVVpSUhneVBTSTFNUzR5TURnek56UXlKU0lnZVRJOUlqQXVOakl4T1RrMU1qWTNKU0lnYVdROUlteHBibVZoY2tkeVlXUnBaVzUwTFRFaVBnb2dJQ0FnSUNBZ0lDQWdJQ0E4YzNSdmNDQnpkRzl3TFdOdmJHOXlQU0lqUVRBM09FWXhJaUJ2Wm1aelpYUTlJakFsSWo0OEwzTjBiM0ErQ2lBZ0lDQWdJQ0FnSUNBZ0lEeHpkRzl3SUhOMGIzQXRZMjlzYjNJOUlpTTJOelF6UkVZaUlHOW1abk5sZEQwaU1UQXdKU0krUEM5emRHOXdQZ29nSUNBZ0lDQWdJRHd2YkdsdVpXRnlSM0poWkdsbGJuUStDaUFnSUNBOEwyUmxabk0rQ2lBZ0lDQThaeUJwWkQwaVVHRm5aUzB4SWlCemRISnZhMlU5SW01dmJtVWlJSE4wY205clpTMTNhV1IwYUQwaU1TSWdabWxzYkQwaWJtOXVaU0lnWm1sc2JDMXlkV3hsUFNKbGRtVnViMlJrSWlCemRISnZhMlV0YkdsdVpXTmhjRDBpY205MWJtUWlDaUFnSUNBZ0lDQnpkSEp2YTJVdGJHbHVaV3B2YVc0OUluSnZkVzVrSWo0S0lDQWdJQ0FnSUNBOFp5QnBaRDBpTVNJZ2RISmhibk5tYjNKdFBTSjBjbUZ1YzJ4aGRHVW9MVFkzTXk0d01EQXdNREFzSUMwME16Z3VNREF3TURBd0tTSWdjM1J5YjJ0bFBTSjFjbXdvSTJ4cGJtVmhja2R5WVdScFpXNTBMVEVwSWlCemRISnZhMlV0ZDJsa2RHZzlJalVpUGdvZ0lDQWdJQ0FnSUNBZ0lDQThjR0YwYUNCa1BTSk5Oekl3TERRME1TQkROamsxTGpZNU9UUTNNU3cwTkRFZ05qYzJMRFEyTUM0Mk9UazBOekVnTmpjMkxEUTROU0JETmpjMkxEVXdPUzR6TURBMU1qa2dOamsxTGpZNU9UUTNNU3cxTWprZ056SXdMRFV5T1NCRE56UTBMak13TURVeU9TdzFNamtnTnpZMExEVXdPUzR6TURBMU1qa2dOelkwTERRNE5TQkROelkwTERRM015NDVNemMyTlRFZ056VTVMamt4TnpVM09DdzBOak11T0RJNE9EQXlJRGMxTXk0eE56Y3hNVGNzTkRVMkxqQTVOemd6TlNJS0lDQWdJQ0FnSUNBZ0lDQWdJQ0FnSUNBZ2FXUTlJazkyWVd3dE15MURiM0I1SWo0OEwzQmhkR2crQ2lBZ0lDQWdJQ0FnUEM5blBnb2dJQ0FnUEM5blBnbzhMM04yWno0PVwiIGFsdD1cIlwiPlxuPC9kaXY+XG5cbjxzdHlsZT5cbiAgICBAa2V5ZnJhbWVzIFNwaW4ge1xuICAgICAgICBmcm9tIHtcbiAgICAgICAgICAgIHRyYW5zZm9ybTogcm90YXRlKDBkZWcpO1xuICAgICAgICB9XG5cbiAgICAgICAgdG8ge1xuICAgICAgICAgICAgdHJhbnNmb3JtOiByb3RhdGUoMzYwZGVnKTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIC5zcGluLWVsZW1lbnQge1xuICAgICAgICBhbmltYXRpb246IFNwaW4gMS41cyBpbmZpbml0ZSBlYXNlLWluLW91dDtcbiAgICB9XG5cbiAgICAuc3Bpbm5lci1jb250YWluZXIge1xuICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICBhbGlnbi1pdGVtczogY2VudGVyO1xuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICAgICAgaGVpZ2h0OiAxMDAlO1xuICAgIH1cbjwvc3R5bGU+Il0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUtJLFdBQVcsbUJBQUssQ0FBQyxBQUNiLElBQUksQUFBQyxDQUFDLEFBQ0YsU0FBUyxDQUFFLE9BQU8sSUFBSSxDQUFDLEFBQzNCLENBQUMsQUFFRCxFQUFFLEFBQUMsQ0FBQyxBQUNBLFNBQVMsQ0FBRSxPQUFPLE1BQU0sQ0FBQyxBQUM3QixDQUFDLEFBQ0wsQ0FBQyxBQUVELGFBQWEsZUFBQyxDQUFDLEFBQ1gsU0FBUyxDQUFFLG1CQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxXQUFXLEFBQzdDLENBQUMsQUFFRCxrQkFBa0IsZUFBQyxDQUFDLEFBQ2hCLE9BQU8sQ0FBRSxJQUFJLENBQ2IsV0FBVyxDQUFFLE1BQU0sQ0FDbkIsZUFBZSxDQUFFLE1BQU0sQ0FDdkIsTUFBTSxDQUFFLElBQUksQUFDaEIsQ0FBQyJ9 */";
	append(document.head, style);
}

function create_main_fragment(component, ctx) {
	var div, img, current;

	return {
		c: function create() {
			div = createElement("div");
			img = createElement("img");
			img.className = "spin-element svelte-1k7b7mr";
			img.src = "data:image/svg+xml;base64,PHN2ZyBjbGFzcz0ic3Bpbi1lbGVtZW50IiB3aWR0aD0iOTRweCIgaGVpZ2h0PSI5NHB4IiB2aWV3Qm94PSIwIDAgOTQgOTQiIHZlcnNpb249IjEuMSIKICAgICB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPgogICAgPGRlZnM+CiAgICAgICAgPGxpbmVhckdyYWRpZW50IHgxPSI4OC42OTU5MzUlIiB5MT0iMTkuOTE4MjQwMyUiIHgyPSI1MS4yMDgzNzQyJSIgeTI9IjAuNjIxOTk1MjY3JSIgaWQ9ImxpbmVhckdyYWRpZW50LTEiPgogICAgICAgICAgICA8c3RvcCBzdG9wLWNvbG9yPSIjQTA3OEYxIiBvZmZzZXQ9IjAlIj48L3N0b3A+CiAgICAgICAgICAgIDxzdG9wIHN0b3AtY29sb3I9IiM2NzQzREYiIG9mZnNldD0iMTAwJSI+PC9zdG9wPgogICAgICAgIDwvbGluZWFyR3JhZGllbnQ+CiAgICA8L2RlZnM+CiAgICA8ZyBpZD0iUGFnZS0xIiBzdHJva2U9Im5vbmUiIHN0cm9rZS13aWR0aD0iMSIgZmlsbD0ibm9uZSIgZmlsbC1ydWxlPSJldmVub2RkIiBzdHJva2UtbGluZWNhcD0icm91bmQiCiAgICAgICBzdHJva2UtbGluZWpvaW49InJvdW5kIj4KICAgICAgICA8ZyBpZD0iMSIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoLTY3My4wMDAwMDAsIC00MzguMDAwMDAwKSIgc3Ryb2tlPSJ1cmwoI2xpbmVhckdyYWRpZW50LTEpIiBzdHJva2Utd2lkdGg9IjUiPgogICAgICAgICAgICA8cGF0aCBkPSJNNzIwLDQ0MSBDNjk1LjY5OTQ3MSw0NDEgNjc2LDQ2MC42OTk0NzEgNjc2LDQ4NSBDNjc2LDUwOS4zMDA1MjkgNjk1LjY5OTQ3MSw1MjkgNzIwLDUyOSBDNzQ0LjMwMDUyOSw1MjkgNzY0LDUwOS4zMDA1MjkgNzY0LDQ4NSBDNzY0LDQ3My45Mzc2NTEgNzU5LjkxNzU3OCw0NjMuODI4ODAyIDc1My4xNzcxMTcsNDU2LjA5NzgzNSIKICAgICAgICAgICAgICAgICAgaWQ9Ik92YWwtMy1Db3B5Ij48L3BhdGg+CiAgICAgICAgPC9nPgogICAgPC9nPgo8L3N2Zz4=";
			img.alt = "";
			addLoc(img, file, 1, 4, 36);
			div.className = "spinner-container svelte-1k7b7mr";
			addLoc(div, file, 0, 0, 0);
		},

		m: function mount(target, anchor) {
			insert(target, div, anchor);
			append(div, img);
			current = true;
		},

		p: noop,

		i: function intro(target, anchor) {
			if (current) { return; }

			this.m(target, anchor);
		},

		o: run,

		d: function destroy$$1(detach) {
			if (detach) {
				detachNode(div);
			}
		}
	};
}

function Auth_spinner(options) {
	this._debugName = '<Auth_spinner>';
	if (!options || (!options.target && !options.root)) {
		throw new Error("'target' is a required option");
	}

	init(this, options);
	this._state = assign({}, options.data);
	this._intro = !!options.intro;

	if (!document.getElementById("svelte-1k7b7mr-style")) { add_css(); }

	this._fragment = create_main_fragment(this, this._state);

	if (options.target) {
		if (options.hydrate) { throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option"); }
		this._fragment.c();
		this._mount(options.target, options.anchor);
	}

	this._intro = true;
}

assign(Auth_spinner.prototype, protoDev);

Auth_spinner.prototype._checkReadOnly = function _checkReadOnly(newState) {
};

/* src/components/qr-code.html generated by Svelte v2.16.1 */

var file$1 = "src/components/qr-code.html";

function add_css$1() {
	var style = createElement("style");
	style.id = 'svelte-1l2qyuy-style';
	style.textContent = ".hive-id-modal__img.svelte-1l2qyuy{display:flex;align-items:center;justify-content:center;max-width:300px;max-height:300px;position:relative;margin:0 auto}.hive-id-modal__img.svelte-1l2qyuy img.svelte-1l2qyuy{width:100%}\n/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicXItY29kZS5odG1sIiwic291cmNlcyI6WyJxci1jb2RlLmh0bWwiXSwic291cmNlc0NvbnRlbnQiOlsiPGRpdj5cbiAgICA8aDMgY2xhc3M9XCJoaXZlLWlkLW1vZGFsX19oZWFkZXJcIj5cbiAgICAgICAgVXNlIHRoZSA8YSBocmVmPVwiL1wiPkhpdmUgSUQgbW9iaWxlPC9hPiBhcHBsaWNhdGlvbiB0byBzY2FuIHRoZSBRUiBjb2RlXG4gICAgPC9oMz5cbiAgICA8ZGl2IGNsYXNzPVwiaGl2ZS1pZC1tb2RhbF9faW1nXCI+XG4gICAgICAgIDxpbWcgc3JjPVwie3FyfVwiIGFsdD1cIlwiPlxuICAgIDwvZGl2PlxuICAgIDxidXR0b24gb246Y2xpY2s9XCJmaXJlKCdjbG9zZScpXCIgY2xhc3M9XCJoaXZlLWlkLW1vZGFsX19idXR0b24gaGl2ZS1pZC1tb2RhbF9fYnV0dG9uX2NhbmNlbFwiPkNhbmNlbDwvYnV0dG9uPlxuPC9kaXY+XG5cbjxzdHlsZT5cbiAgICAuaGl2ZS1pZC1tb2RhbF9faW1nIHtcbiAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgICAgIG1heC13aWR0aDogMzAwcHg7XG4gICAgICAgIG1heC1oZWlnaHQ6IDMwMHB4O1xuICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgICAgIG1hcmdpbjogMCBhdXRvO1xuICAgIH1cblxuICAgIC5oaXZlLWlkLW1vZGFsX19pbWcgaW1nIHtcbiAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgfVxuPC9zdHlsZT4iXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBV0ksbUJBQW1CLGVBQUMsQ0FBQyxBQUNqQixPQUFPLENBQUUsSUFBSSxDQUNiLFdBQVcsQ0FBRSxNQUFNLENBQ25CLGVBQWUsQ0FBRSxNQUFNLENBQ3ZCLFNBQVMsQ0FBRSxLQUFLLENBQ2hCLFVBQVUsQ0FBRSxLQUFLLENBQ2pCLFFBQVEsQ0FBRSxRQUFRLENBQ2xCLE1BQU0sQ0FBRSxDQUFDLENBQUMsSUFBSSxBQUNsQixDQUFDLEFBRUQsa0NBQW1CLENBQUMsR0FBRyxlQUFDLENBQUMsQUFDckIsS0FBSyxDQUFFLElBQUksQUFDZixDQUFDIn0= */";
	append(document.head, style);
}

function create_main_fragment$1(component, ctx) {
	var div1, h3, text0, a, text2, text3, div0, img, text4, button, current;

	function click_handler(event) {
		component.fire('close');
	}

	return {
		c: function create() {
			div1 = createElement("div");
			h3 = createElement("h3");
			text0 = createText("Use the ");
			a = createElement("a");
			a.textContent = "Hive ID mobile";
			text2 = createText(" application to scan the QR code");
			text3 = createText("\n    ");
			div0 = createElement("div");
			img = createElement("img");
			text4 = createText("\n    ");
			button = createElement("button");
			button.textContent = "Cancel";
			a.href = "/";
			addLoc(a, file$1, 2, 16, 61);
			h3.className = "hive-id-modal__header";
			addLoc(h3, file$1, 1, 4, 10);
			img.src = ctx.qr;
			img.alt = "";
			img.className = "svelte-1l2qyuy";
			addLoc(img, file$1, 5, 8, 179);
			div0.className = "hive-id-modal__img svelte-1l2qyuy";
			addLoc(div0, file$1, 4, 4, 138);
			addListener(button, "click", click_handler);
			button.className = "hive-id-modal__button hive-id-modal__button_cancel";
			addLoc(button, file$1, 7, 4, 218);
			addLoc(div1, file$1, 0, 0, 0);
		},

		m: function mount(target, anchor) {
			insert(target, div1, anchor);
			append(div1, h3);
			append(h3, text0);
			append(h3, a);
			append(h3, text2);
			append(div1, text3);
			append(div1, div0);
			append(div0, img);
			append(div1, text4);
			append(div1, button);
			current = true;
		},

		p: function update(changed, ctx) {
			if (changed.qr) {
				img.src = ctx.qr;
			}
		},

		i: function intro(target, anchor) {
			if (current) { return; }

			this.m(target, anchor);
		},

		o: run,

		d: function destroy$$1(detach) {
			if (detach) {
				detachNode(div1);
			}

			removeListener(button, "click", click_handler);
		}
	};
}

function Qr_code(options) {
	this._debugName = '<Qr_code>';
	if (!options || (!options.target && !options.root)) {
		throw new Error("'target' is a required option");
	}

	init(this, options);
	this._state = assign({}, options.data);
	if (!('qr' in this._state)) { console.warn("<Qr_code> was created without expected data property 'qr'"); }
	this._intro = !!options.intro;

	if (!document.getElementById("svelte-1l2qyuy-style")) { add_css$1(); }

	this._fragment = create_main_fragment$1(this, this._state);

	if (options.target) {
		if (options.hydrate) { throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option"); }
		this._fragment.c();
		this._mount(options.target, options.anchor);
	}

	this._intro = true;
}

assign(Qr_code.prototype, protoDev);

Qr_code.prototype._checkReadOnly = function _checkReadOnly(newState) {
};

/* src/components/modal.html generated by Svelte v2.16.1 */

function data() {
    return {
        intervalId: null,
        state: {
            pending: false,
            expire: false,
            scanned: false,
        },
        data: null
    }
}
var methods = {
    onScanned: function onScanned() {
        this.setDeep('state.scanned', true);
    },
    onComplete: function onComplete(data) {
        this.store.get().callback(data);
        this.destroy();
    },
    onRetry: function onRetry() {
        this.set({
            intervalId: null,
            state: {pending: false, expire: false, scanned: false},
            data: null
        });

        this.downloadQR();
    },
    downloadQR: function downloadQR() {
        var this$1 = this;

        this.setDeep('state.pending', true);

        createSession(this.store.get().apiId, function (res) {
            this$1.setDeep('state.pending', false);
            console.log(res.data.session_id);
            this$1.set({data: res.data});

            this$1.intervalId = pollingRequest(
                res.data.session_id,
                this$1.onComplete.bind(this$1),
                function () {
                    setTimeout(function () {
                        clearInterval(this$1.intervalId);
                        this$1.setDeep('state.expire', true);
                    }, +res.data.active_until - (+Date.now()));
                },
                this$1.onScanned.bind(this$1)
            );
        });
    },
    onExpire: function onExpire() {
        this.setDeep('state.expire', true);
    },
    onClickOverlay: function onClickOverlay(ref) {
        var currentTarget = ref.currentTarget;
        var target = ref.target;

        currentTarget === target && this.destroy();
    },
    setDeep: setDeep,
};

function oncreate() {
    this.downloadQR();
}
function ondestroy() {
    clearInterval(this.intervalId);
}
var file$2 = "src/components/modal.html";

function add_css$2() {
	var style = createElement("style");
	style.id = 'svelte-18twk71-style';
	style.textContent = ".hive-id-modal__overlay.svelte-18twk71{position:fixed;display:flex;align-items:center;justify-content:center;top:0;left:0;bottom:0;right:0;z-index:var(--hive-id-modal-z-index, 100001);background:var(--hive-id-modal-overlay-background, rgba(0, 0, 0, 0.7))}.hive-id-modal__content.svelte-18twk71{position:relative;display:flex;align-items:center;justify-content:center;width:100%;height:100%;max-width:var(--hive-id-modal-content-max-width, 570px);max-height:var(--hive-id-modal-content-max-height, 570px);margin:var(--hive-id-content-margin, 20px 15px);padding:var(--hive-id-content-padding, 40px);background:var(--hive-id-modal-content-background, rgb(255, 255, 255));border-radius:var(--hive-id-modal-content-border-radius, 4px);outline:none}.hive-id-modal__content h3{margin-bottom:28px;font-size:var(--hive-id-modal-header-font-size, 18px);color:var(--hive-id-modal-header-color, #3E466D);line-height:28px;text-align:center;font-weight:500}.hive-id-modal__content button{background:none;border:none;outline:none;box-shadow:none;cursor:pointer}.hive-id-modal__header a{color:#6743DF}.hive-id-modal__button_cancel{display:block;margin:var(--hive-id-modal-button-cancel-margin, 38px auto 0);color:var(--hive-id-modal-button-cancel-color, #6743DF);font-size:18px;line-height:28px}.hive-id-modal__button_retry.svelte-18twk71{background:var(--hive-id-modal-button-retry-background, linear-gradient(45deg, #925DFB 0%, #6743DF 100%));padding:var(--hive-id-modal-button-retry-padding, 17px 66px);margin:var(--hive-id-modal-button-retry-margin, 90px auto 52px);display:block;border-radius:29px;font-size:20px;color:var(--hive-id-modal-button-retry-color, #FFFFFF);letter-spacing:0;text-align:left;line-height:24px}\n/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibW9kYWwuaHRtbCIsInNvdXJjZXMiOlsibW9kYWwuaHRtbCJdLCJzb3VyY2VzQ29udGVudCI6WyI8ZGl2IGNsYXNzPVwiaGl2ZS1pZC1tb2RhbF9fb3ZlcmxheVwiIG9uOmNsaWNrPVwib25DbGlja092ZXJsYXkoZXZlbnQpXCI+XG4gICAgPGRpdiBjbGFzcz1cImhpdmUtaWQtbW9kYWxfX2NvbnRlbnRcIj5cbiAgICAgICAgeyNpZiBzdGF0ZS5wZW5kaW5nICYmICFzdGF0ZS5leHBpcmV9XG4gICAgICAgIDxBdXRoU3Bpbm5lcj48L0F1dGhTcGlubmVyPlxuICAgICAgICB7OmVsc2VpZiBkYXRhICE9PSBudWxsICYmICFzdGF0ZS5leHBpcmUgJiYgIXN0YXRlLnNjYW5uZWR9XG4gICAgICAgIDxRUkNvZGUgcXI9XCJ7ZGF0YS5xcl9jb2RlfVwiIG9uOmNsb3NlPVwiZGVzdHJveSgpXCI+PC9RUkNvZGU+XG4gICAgICAgIHs6ZWxzZWlmIHN0YXRlLmV4cGlyZX1cbiAgICAgICAgPGRpdj5cbiAgICAgICAgICAgIDxoMz5JdCBsb29rcyBsaWtlIHlvdSBuZWVkIG1vcmUgdGltZS48YnIvPlBsZWFzZSBjbGljayByZXRyeSB0byBnZXQgYSBuZXcgUVIgY29kZS48L2gzPlxuICAgICAgICAgICAgPGJ1dHRvbiBvbjpjbGljaz1cIm9uUmV0cnkoKVwiIGNsYXNzPVwiaGl2ZS1pZC1tb2RhbF9fYnV0dG9uIGhpdmUtaWQtbW9kYWxfX2J1dHRvbl9yZXRyeVwiPlJldHJ5PC9idXR0b24+XG4gICAgICAgICAgICA8YnV0dG9uIG9uOmNsaWNrPVwiZGVzdHJveSgpXCIgY2xhc3M9XCJoaXZlLWlkLW1vZGFsX19idXR0b25fY2FuY2VsXCI+Q2FuY2VsPC9idXR0b24+XG4gICAgICAgIDwvZGl2PlxuICAgICAgICB7OmVsc2VpZiBzdGF0ZS5zY2FubmVkfVxuICAgICAgICA8QXV0aFNwaW5uZXI+PC9BdXRoU3Bpbm5lcj5cbiAgICAgICAgey9pZn1cbiAgICA8L2Rpdj5cbjwvZGl2PlxuXG48c2NyaXB0PlxuICAgIGltcG9ydCB7c2V0RGVlcH0gZnJvbSAnc3ZlbHRlLWV4dHJhcyc7XG4gICAgaW1wb3J0IHtjcmVhdGVTZXNzaW9uLCBwb2xsaW5nUmVxdWVzdH0gZnJvbSAnLi4vbGlicy9yZXF1ZXN0JztcbiAgICBpbXBvcnQgQXV0aFNwaW5uZXIgZnJvbSAnLi9hdXRoLXNwaW5uZXIuaHRtbCc7XG4gICAgaW1wb3J0IFFSQ29kZSBmcm9tICcuL3FyLWNvZGUuaHRtbCc7XG5cbiAgICBjb25zdCBkZWZhdWx0RGF0YSA9IHtcbiAgICAgICAgaW50ZXJ2YWxJZDogbnVsbCxcbiAgICAgICAgc3RhdGU6IHtcbiAgICAgICAgICAgIHBlbmRpbmc6IGZhbHNlLFxuICAgICAgICAgICAgZXhwaXJlOiBmYWxzZSxcbiAgICAgICAgICAgIHNjYW5uZWQ6IGZhbHNlLFxuICAgICAgICB9LFxuICAgICAgICBkYXRhOiBudWxsXG4gICAgfTtcblxuICAgIGV4cG9ydCBkZWZhdWx0IHtcbiAgICAgICAgY29tcG9uZW50czoge1xuICAgICAgICAgICAgQXV0aFNwaW5uZXIsXG4gICAgICAgICAgICBRUkNvZGVcbiAgICAgICAgfSxcbiAgICAgICAgZGF0YSgpIHtcbiAgICAgICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICAgICAgaW50ZXJ2YWxJZDogbnVsbCxcbiAgICAgICAgICAgICAgICBzdGF0ZToge1xuICAgICAgICAgICAgICAgICAgICBwZW5kaW5nOiBmYWxzZSxcbiAgICAgICAgICAgICAgICAgICAgZXhwaXJlOiBmYWxzZSxcbiAgICAgICAgICAgICAgICAgICAgc2Nhbm5lZDogZmFsc2UsXG4gICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICBkYXRhOiBudWxsXG4gICAgICAgICAgICB9XG4gICAgICAgIH0sXG4gICAgICAgIG9uY3JlYXRlKCkge1xuICAgICAgICAgICAgdGhpcy5kb3dubG9hZFFSKCk7XG4gICAgICAgIH0sXG4gICAgICAgIG9uZGVzdHJveSgpIHtcbiAgICAgICAgICAgIGNsZWFySW50ZXJ2YWwodGhpcy5pbnRlcnZhbElkKTtcbiAgICAgICAgfSxcbiAgICAgICAgbWV0aG9kczoge1xuICAgICAgICAgICAgb25TY2FubmVkKCkge1xuICAgICAgICAgICAgICAgIHRoaXMuc2V0RGVlcCgnc3RhdGUuc2Nhbm5lZCcsIHRydWUpO1xuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIG9uQ29tcGxldGUoZGF0YSkge1xuICAgICAgICAgICAgICAgIHRoaXMuc3RvcmUuZ2V0KCkuY2FsbGJhY2soZGF0YSk7XG4gICAgICAgICAgICAgICAgdGhpcy5kZXN0cm95KCk7XG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgb25SZXRyeSgpIHtcbiAgICAgICAgICAgICAgICB0aGlzLnNldCh7XG4gICAgICAgICAgICAgICAgICAgIGludGVydmFsSWQ6IG51bGwsXG4gICAgICAgICAgICAgICAgICAgIHN0YXRlOiB7cGVuZGluZzogZmFsc2UsIGV4cGlyZTogZmFsc2UsIHNjYW5uZWQ6IGZhbHNlfSxcbiAgICAgICAgICAgICAgICAgICAgZGF0YTogbnVsbFxuICAgICAgICAgICAgICAgIH0pO1xuXG4gICAgICAgICAgICAgICAgdGhpcy5kb3dubG9hZFFSKCk7XG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgZG93bmxvYWRRUigpIHtcbiAgICAgICAgICAgICAgICB0aGlzLnNldERlZXAoJ3N0YXRlLnBlbmRpbmcnLCB0cnVlKTtcblxuICAgICAgICAgICAgICAgIGNyZWF0ZVNlc3Npb24odGhpcy5zdG9yZS5nZXQoKS5hcGlJZCwgKHJlcykgPT4ge1xuICAgICAgICAgICAgICAgICAgICB0aGlzLnNldERlZXAoJ3N0YXRlLnBlbmRpbmcnLCBmYWxzZSk7XG4gICAgICAgICAgICAgICAgICAgIGNvbnNvbGUubG9nKHJlcy5kYXRhLnNlc3Npb25faWQpO1xuICAgICAgICAgICAgICAgICAgICB0aGlzLnNldCh7ZGF0YTogcmVzLmRhdGF9KTtcblxuICAgICAgICAgICAgICAgICAgICB0aGlzLmludGVydmFsSWQgPSBwb2xsaW5nUmVxdWVzdChcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlcy5kYXRhLnNlc3Npb25faWQsXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLm9uQ29tcGxldGUuYmluZCh0aGlzKSxcbiAgICAgICAgICAgICAgICAgICAgICAgICgpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBzZXRUaW1lb3V0KCgpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY2xlYXJJbnRlcnZhbCh0aGlzLmludGVydmFsSWQpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnNldERlZXAoJ3N0YXRlLmV4cGlyZScsIHRydWUpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sICtyZXMuZGF0YS5hY3RpdmVfdW50aWwgLSAoK0RhdGUubm93KCkpKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLm9uU2Nhbm5lZC5iaW5kKHRoaXMpXG4gICAgICAgICAgICAgICAgICAgICk7XG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgb25FeHBpcmUoKSB7XG4gICAgICAgICAgICAgICAgdGhpcy5zZXREZWVwKCdzdGF0ZS5leHBpcmUnLCB0cnVlKTtcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICBvbkNsaWNrT3ZlcmxheSh7Y3VycmVudFRhcmdldCwgdGFyZ2V0fSkge1xuICAgICAgICAgICAgICAgIGN1cnJlbnRUYXJnZXQgPT09IHRhcmdldCAmJiB0aGlzLmRlc3Ryb3koKTtcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICBzZXREZWVwLFxuICAgICAgICB9XG4gICAgfVxuPC9zY3JpcHQ+XG5cbjxzdHlsZT5cbiAgICAuaGl2ZS1pZC1tb2RhbF9fb3ZlcmxheSB7XG4gICAgICAgIHBvc2l0aW9uOiBmaXhlZDtcbiAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgICAgIHRvcDogMDtcbiAgICAgICAgbGVmdDogMDtcbiAgICAgICAgYm90dG9tOiAwO1xuICAgICAgICByaWdodDogMDtcbiAgICAgICAgei1pbmRleDogdmFyKC0taGl2ZS1pZC1tb2RhbC16LWluZGV4LCAxMDAwMDEpO1xuICAgICAgICBiYWNrZ3JvdW5kOiB2YXIoLS1oaXZlLWlkLW1vZGFsLW92ZXJsYXktYmFja2dyb3VuZCwgcmdiYSgwLCAwLCAwLCAwLjcpKTtcbiAgICB9XG5cbiAgICAuaGl2ZS1pZC1tb2RhbF9fY29udGVudCB7XG4gICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gICAgICAgIHdpZHRoOiAxMDAlO1xuICAgICAgICBoZWlnaHQ6IDEwMCU7XG4gICAgICAgIG1heC13aWR0aDogdmFyKC0taGl2ZS1pZC1tb2RhbC1jb250ZW50LW1heC13aWR0aCwgNTcwcHgpO1xuICAgICAgICBtYXgtaGVpZ2h0OiB2YXIoLS1oaXZlLWlkLW1vZGFsLWNvbnRlbnQtbWF4LWhlaWdodCwgNTcwcHgpO1xuICAgICAgICBtYXJnaW46IHZhcigtLWhpdmUtaWQtY29udGVudC1tYXJnaW4sIDIwcHggMTVweCk7XG4gICAgICAgIHBhZGRpbmc6IHZhcigtLWhpdmUtaWQtY29udGVudC1wYWRkaW5nLCA0MHB4KTtcbiAgICAgICAgYmFja2dyb3VuZDogdmFyKC0taGl2ZS1pZC1tb2RhbC1jb250ZW50LWJhY2tncm91bmQsIHJnYigyNTUsIDI1NSwgMjU1KSk7XG4gICAgICAgIGJvcmRlci1yYWRpdXM6IHZhcigtLWhpdmUtaWQtbW9kYWwtY29udGVudC1ib3JkZXItcmFkaXVzLCA0cHgpO1xuICAgICAgICBvdXRsaW5lOiBub25lO1xuICAgIH1cblxuICAgIDpnbG9iYWwoLmhpdmUtaWQtbW9kYWxfX2NvbnRlbnQgaDMpIHtcbiAgICAgICAgbWFyZ2luLWJvdHRvbTogMjhweDtcbiAgICAgICAgZm9udC1zaXplOiB2YXIoLS1oaXZlLWlkLW1vZGFsLWhlYWRlci1mb250LXNpemUsIDE4cHgpO1xuICAgICAgICBjb2xvcjogdmFyKC0taGl2ZS1pZC1tb2RhbC1oZWFkZXItY29sb3IsICMzRTQ2NkQpO1xuICAgICAgICBsaW5lLWhlaWdodDogMjhweDtcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgICAgICBmb250LXdlaWdodDogNTAwO1xuICAgIH1cblxuICAgIDpnbG9iYWwoLmhpdmUtaWQtbW9kYWxfX2NvbnRlbnQgYnV0dG9uKSB7XG4gICAgICAgIGJhY2tncm91bmQ6IG5vbmU7XG4gICAgICAgIGJvcmRlcjogbm9uZTtcbiAgICAgICAgb3V0bGluZTogbm9uZTtcbiAgICAgICAgYm94LXNoYWRvdzogbm9uZTtcbiAgICAgICAgY3Vyc29yOiBwb2ludGVyO1xuICAgIH1cblxuICAgIDpnbG9iYWwoLmhpdmUtaWQtbW9kYWxfX2hlYWRlciBhKSB7XG4gICAgICAgIGNvbG9yOiAjNjc0M0RGO1xuICAgIH1cblxuICAgIDpnbG9iYWwoLmhpdmUtaWQtbW9kYWxfX2J1dHRvbl9jYW5jZWwpIHtcbiAgICAgICAgZGlzcGxheTogYmxvY2s7XG4gICAgICAgIG1hcmdpbjogdmFyKC0taGl2ZS1pZC1tb2RhbC1idXR0b24tY2FuY2VsLW1hcmdpbiwgMzhweCBhdXRvIDApO1xuICAgICAgICBjb2xvcjogdmFyKC0taGl2ZS1pZC1tb2RhbC1idXR0b24tY2FuY2VsLWNvbG9yLCAjNjc0M0RGKTtcbiAgICAgICAgZm9udC1zaXplOiAxOHB4O1xuICAgICAgICBsaW5lLWhlaWdodDogMjhweDtcbiAgICB9XG5cbiAgICAuaGl2ZS1pZC1tb2RhbF9fYnV0dG9uX3JldHJ5IHtcbiAgICAgICAgYmFja2dyb3VuZDogdmFyKC0taGl2ZS1pZC1tb2RhbC1idXR0b24tcmV0cnktYmFja2dyb3VuZCwgbGluZWFyLWdyYWRpZW50KDQ1ZGVnLCAjOTI1REZCIDAlLCAjNjc0M0RGIDEwMCUpKTtcbiAgICAgICAgcGFkZGluZzogdmFyKC0taGl2ZS1pZC1tb2RhbC1idXR0b24tcmV0cnktcGFkZGluZywgMTdweCA2NnB4KTtcbiAgICAgICAgbWFyZ2luOiB2YXIoLS1oaXZlLWlkLW1vZGFsLWJ1dHRvbi1yZXRyeS1tYXJnaW4sIDkwcHggYXV0byA1MnB4KTtcbiAgICAgICAgZGlzcGxheTogYmxvY2s7XG4gICAgICAgIGJvcmRlci1yYWRpdXM6IDI5cHg7XG4gICAgICAgIGZvbnQtc2l6ZTogMjBweDtcbiAgICAgICAgY29sb3I6IHZhcigtLWhpdmUtaWQtbW9kYWwtYnV0dG9uLXJldHJ5LWNvbG9yLCAjRkZGRkZGKTtcbiAgICAgICAgbGV0dGVyLXNwYWNpbmc6IDA7XG4gICAgICAgIHRleHQtYWxpZ246IGxlZnQ7XG4gICAgICAgIGxpbmUtaGVpZ2h0OiAyNHB4O1xuICAgIH1cbjwvc3R5bGU+XG4iXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBMEdJLHVCQUF1QixlQUFDLENBQUMsQUFDckIsUUFBUSxDQUFFLEtBQUssQ0FDZixPQUFPLENBQUUsSUFBSSxDQUNiLFdBQVcsQ0FBRSxNQUFNLENBQ25CLGVBQWUsQ0FBRSxNQUFNLENBQ3ZCLEdBQUcsQ0FBRSxDQUFDLENBQ04sSUFBSSxDQUFFLENBQUMsQ0FDUCxNQUFNLENBQUUsQ0FBQyxDQUNULEtBQUssQ0FBRSxDQUFDLENBQ1IsT0FBTyxDQUFFLElBQUksdUJBQXVCLENBQUMsT0FBTyxDQUFDLENBQzdDLFVBQVUsQ0FBRSxJQUFJLGtDQUFrQyxDQUFDLG1CQUFtQixDQUFDLEFBQzNFLENBQUMsQUFFRCx1QkFBdUIsZUFBQyxDQUFDLEFBQ3JCLFFBQVEsQ0FBRSxRQUFRLENBQ2xCLE9BQU8sQ0FBRSxJQUFJLENBQ2IsV0FBVyxDQUFFLE1BQU0sQ0FDbkIsZUFBZSxDQUFFLE1BQU0sQ0FDdkIsS0FBSyxDQUFFLElBQUksQ0FDWCxNQUFNLENBQUUsSUFBSSxDQUNaLFNBQVMsQ0FBRSxJQUFJLGlDQUFpQyxDQUFDLE1BQU0sQ0FBQyxDQUN4RCxVQUFVLENBQUUsSUFBSSxrQ0FBa0MsQ0FBQyxNQUFNLENBQUMsQ0FDMUQsTUFBTSxDQUFFLElBQUksd0JBQXdCLENBQUMsVUFBVSxDQUFDLENBQ2hELE9BQU8sQ0FBRSxJQUFJLHlCQUF5QixDQUFDLEtBQUssQ0FBQyxDQUM3QyxVQUFVLENBQUUsSUFBSSxrQ0FBa0MsQ0FBQyxtQkFBbUIsQ0FBQyxDQUN2RSxhQUFhLENBQUUsSUFBSSxxQ0FBcUMsQ0FBQyxJQUFJLENBQUMsQ0FDOUQsT0FBTyxDQUFFLElBQUksQUFDakIsQ0FBQyxBQUVPLDBCQUEwQixBQUFFLENBQUMsQUFDakMsYUFBYSxDQUFFLElBQUksQ0FDbkIsU0FBUyxDQUFFLElBQUksZ0NBQWdDLENBQUMsS0FBSyxDQUFDLENBQ3RELEtBQUssQ0FBRSxJQUFJLDRCQUE0QixDQUFDLFFBQVEsQ0FBQyxDQUNqRCxXQUFXLENBQUUsSUFBSSxDQUNqQixVQUFVLENBQUUsTUFBTSxDQUNsQixXQUFXLENBQUUsR0FBRyxBQUNwQixDQUFDLEFBRU8sOEJBQThCLEFBQUUsQ0FBQyxBQUNyQyxVQUFVLENBQUUsSUFBSSxDQUNoQixNQUFNLENBQUUsSUFBSSxDQUNaLE9BQU8sQ0FBRSxJQUFJLENBQ2IsVUFBVSxDQUFFLElBQUksQ0FDaEIsTUFBTSxDQUFFLE9BQU8sQUFDbkIsQ0FBQyxBQUVPLHdCQUF3QixBQUFFLENBQUMsQUFDL0IsS0FBSyxDQUFFLE9BQU8sQUFDbEIsQ0FBQyxBQUVPLDZCQUE2QixBQUFFLENBQUMsQUFDcEMsT0FBTyxDQUFFLEtBQUssQ0FDZCxNQUFNLENBQUUsSUFBSSxvQ0FBb0MsQ0FBQyxZQUFZLENBQUMsQ0FDOUQsS0FBSyxDQUFFLElBQUksbUNBQW1DLENBQUMsUUFBUSxDQUFDLENBQ3hELFNBQVMsQ0FBRSxJQUFJLENBQ2YsV0FBVyxDQUFFLElBQUksQUFDckIsQ0FBQyxBQUVELDRCQUE0QixlQUFDLENBQUMsQUFDMUIsVUFBVSxDQUFFLElBQUksdUNBQXVDLENBQUMsaURBQWlELENBQUMsQ0FDMUcsT0FBTyxDQUFFLElBQUksb0NBQW9DLENBQUMsVUFBVSxDQUFDLENBQzdELE1BQU0sQ0FBRSxJQUFJLG1DQUFtQyxDQUFDLGVBQWUsQ0FBQyxDQUNoRSxPQUFPLENBQUUsS0FBSyxDQUNkLGFBQWEsQ0FBRSxJQUFJLENBQ25CLFNBQVMsQ0FBRSxJQUFJLENBQ2YsS0FBSyxDQUFFLElBQUksa0NBQWtDLENBQUMsUUFBUSxDQUFDLENBQ3ZELGNBQWMsQ0FBRSxDQUFDLENBQ2pCLFVBQVUsQ0FBRSxJQUFJLENBQ2hCLFdBQVcsQ0FBRSxJQUFJLEFBQ3JCLENBQUMifQ== */";
	append(document.head, style);
}

function create_main_fragment$2(component, ctx) {
	var div1, div0, current_block_type_index, if_block, current;

	var if_block_creators = [
		create_if_block,
		create_if_block_1,
		create_if_block_2,
		create_if_block_3
	];

	var if_blocks = [];

	function select_block_type(ctx) {
		if (ctx.state.pending && !ctx.state.expire) { return 0; }
		if (ctx.data !== null && !ctx.state.expire && !ctx.state.scanned) { return 1; }
		if (ctx.state.expire) { return 2; }
		if (ctx.state.scanned) { return 3; }
		return -1;
	}

	if (~(current_block_type_index = select_block_type(ctx))) {
		if_block = if_blocks[current_block_type_index] = if_block_creators[current_block_type_index](component, ctx);
	}

	function click_handler(event) {
		component.onClickOverlay(event);
	}

	return {
		c: function create() {
			div1 = createElement("div");
			div0 = createElement("div");
			if (if_block) { if_block.c(); }
			div0.className = "hive-id-modal__content svelte-18twk71";
			addLoc(div0, file$2, 1, 4, 74);
			addListener(div1, "click", click_handler);
			div1.className = "hive-id-modal__overlay svelte-18twk71";
			addLoc(div1, file$2, 0, 0, 0);
		},

		m: function mount(target, anchor) {
			insert(target, div1, anchor);
			append(div1, div0);
			if (~current_block_type_index) { if_blocks[current_block_type_index].m(div0, null); }
			current = true;
		},

		p: function update(changed, ctx) {
			var previous_block_index = current_block_type_index;
			current_block_type_index = select_block_type(ctx);
			if (current_block_type_index === previous_block_index) {
				if (~current_block_type_index) { if_blocks[current_block_type_index].p(changed, ctx); }
			} else {
				if (if_block) {
					if_block.o(function() {
						if_blocks[previous_block_index].d(1);
						if_blocks[previous_block_index] = null;
					});
				}

				if (~current_block_type_index) {
					if_block = if_blocks[current_block_type_index];
					if (!if_block) {
						if_block = if_blocks[current_block_type_index] = if_block_creators[current_block_type_index](component, ctx);
						if_block.c();
					}
					if_block.m(div0, null);
				} else {
					if_block = null;
				}
			}
		},

		i: function intro(target, anchor) {
			if (current) { return; }

			this.m(target, anchor);
		},

		o: function outro(outrocallback) {
			if (!current) { return; }

			if (if_block) { if_block.o(outrocallback); }
			else { outrocallback(); }

			current = false;
		},

		d: function destroy$$1(detach) {
			if (detach) {
				detachNode(div1);
			}

			if (~current_block_type_index) { if_blocks[current_block_type_index].d(); }
			removeListener(div1, "click", click_handler);
		}
	};
}

// (13:31) 
function create_if_block_3(component, ctx) {
	var current;

	var authspinner = new Auth_spinner({
		root: component.root,
		store: component.store
	});

	return {
		c: function create() {
			authspinner._fragment.c();
		},

		m: function mount(target, anchor) {
			authspinner._mount(target, anchor);
			current = true;
		},

		p: noop,

		i: function intro(target, anchor) {
			if (current) { return; }

			this.m(target, anchor);
		},

		o: function outro(outrocallback) {
			if (!current) { return; }

			if (authspinner) { authspinner._fragment.o(outrocallback); }
			current = false;
		},

		d: function destroy$$1(detach) {
			authspinner.destroy(detach);
		}
	};
}

// (7:30) 
function create_if_block_2(component, ctx) {
	var div, h3, text0, br, text1, text2, button0, text4, button1, current;

	function click_handler(event) {
		component.onRetry();
	}

	function click_handler_1(event) {
		component.destroy();
	}

	return {
		c: function create() {
			div = createElement("div");
			h3 = createElement("h3");
			text0 = createText("It looks like you need more time.");
			br = createElement("br");
			text1 = createText("Please click retry to get a new QR code.");
			text2 = createText("\n            ");
			button0 = createElement("button");
			button0.textContent = "Retry";
			text4 = createText("\n            ");
			button1 = createElement("button");
			button1.textContent = "Cancel";
			addLoc(br, file$2, 8, 49, 420);
			addLoc(h3, file$2, 8, 12, 383);
			addListener(button0, "click", click_handler);
			button0.className = "hive-id-modal__button hive-id-modal__button_retry svelte-18twk71";
			addLoc(button0, file$2, 9, 12, 483);
			addListener(button1, "click", click_handler_1);
			button1.className = "hive-id-modal__button_cancel";
			addLoc(button1, file$2, 10, 12, 597);
			addLoc(div, file$2, 7, 8, 365);
		},

		m: function mount(target, anchor) {
			insert(target, div, anchor);
			append(div, h3);
			append(h3, text0);
			append(h3, br);
			append(h3, text1);
			append(div, text2);
			append(div, button0);
			append(div, text4);
			append(div, button1);
			current = true;
		},

		p: noop,

		i: function intro(target, anchor) {
			if (current) { return; }

			this.m(target, anchor);
		},

		o: run,

		d: function destroy$$1(detach) {
			if (detach) {
				detachNode(div);
			}

			removeListener(button0, "click", click_handler);
			removeListener(button1, "click", click_handler_1);
		}
	};
}

// (5:66) 
function create_if_block_1(component, ctx) {
	var current;

	var qrcode_initial_data = { qr: ctx.data.qr_code };
	var qrcode = new Qr_code({
		root: component.root,
		store: component.store,
		data: qrcode_initial_data
	});

	qrcode.on("close", function(event) {
		component.destroy();
	});

	return {
		c: function create() {
			qrcode._fragment.c();
		},

		m: function mount(target, anchor) {
			qrcode._mount(target, anchor);
			current = true;
		},

		p: function update(changed, ctx) {
			var qrcode_changes = {};
			if (changed.data) { qrcode_changes.qr = ctx.data.qr_code; }
			qrcode._set(qrcode_changes);
		},

		i: function intro(target, anchor) {
			if (current) { return; }

			this.m(target, anchor);
		},

		o: function outro(outrocallback) {
			if (!current) { return; }

			if (qrcode) { qrcode._fragment.o(outrocallback); }
			current = false;
		},

		d: function destroy$$1(detach) {
			qrcode.destroy(detach);
		}
	};
}

// (3:8) {#if state.pending && !state.expire}
function create_if_block(component, ctx) {
	var current;

	var authspinner = new Auth_spinner({
		root: component.root,
		store: component.store
	});

	return {
		c: function create() {
			authspinner._fragment.c();
		},

		m: function mount(target, anchor) {
			authspinner._mount(target, anchor);
			current = true;
		},

		p: noop,

		i: function intro(target, anchor) {
			if (current) { return; }

			this.m(target, anchor);
		},

		o: function outro(outrocallback) {
			if (!current) { return; }

			if (authspinner) { authspinner._fragment.o(outrocallback); }
			current = false;
		},

		d: function destroy$$1(detach) {
			authspinner.destroy(detach);
		}
	};
}

function Modal(options) {
	var this$1 = this;

	this._debugName = '<Modal>';
	if (!options || (!options.target && !options.root)) {
		throw new Error("'target' is a required option");
	}

	init(this, options);
	this._state = assign(data(), options.data);
	if (!('state' in this._state)) { console.warn("<Modal> was created without expected data property 'state'"); }
	if (!('data' in this._state)) { console.warn("<Modal> was created without expected data property 'data'"); }
	this._intro = !!options.intro;

	this._handlers.destroy = [ondestroy];

	if (!document.getElementById("svelte-18twk71-style")) { add_css$2(); }

	this._fragment = create_main_fragment$2(this, this._state);

	this.root._oncreate.push(function () {
		oncreate.call(this$1);
		this$1.fire("update", { changed: assignTrue({}, this$1._state), current: this$1._state });
	});

	if (options.target) {
		if (options.hydrate) { throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option"); }
		this._fragment.c();
		this._mount(options.target, options.anchor);

		flush(this);
	}

	this._intro = true;
}

assign(Modal.prototype, protoDev);
assign(Modal.prototype, methods);

Modal.prototype._checkReadOnly = function _checkReadOnly(newState) {
};

function Store(state, options) {
	this._handlers = {};
	this._dependents = [];

	this._computed = blankObject();
	this._sortedComputedProperties = [];

	this._state = assign({}, state);
	this._differs = options && options.immutable ? _differsImmutable : _differs;
}

assign(Store.prototype, {
	_add: function _add(component, props) {
		this._dependents.push({
			component: component,
			props: props
		});
	},

	_init: function _init(props) {
		var state = {};
		for (var i = 0; i < props.length; i += 1) {
			var prop = props[i];
			state['$' + prop] = this._state[prop];
		}
		return state;
	},

	_remove: function _remove(component) {
		var i = this._dependents.length;
		while (i--) {
			if (this._dependents[i].component === component) {
				this._dependents.splice(i, 1);
				return;
			}
		}
	},

	_set: function _set$$1(newState, changed) {
		var this$1 = this;

		var previous = this._state;
		this._state = assign(assign({}, previous), newState);

		for (var i = 0; i < this._sortedComputedProperties.length; i += 1) {
			this._sortedComputedProperties[i].update(this._state, changed);
		}

		this.fire('state', {
			changed: changed,
			previous: previous,
			current: this._state
		});

		this._dependents
			.filter(function (dependent) {
				var componentState = {};
				var dirty = false;

				for (var j = 0; j < dependent.props.length; j += 1) {
					var prop = dependent.props[j];
					if (prop in changed) {
						componentState['$' + prop] = this$1._state[prop];
						dirty = true;
					}
				}

				if (dirty) {
					dependent.component._stage(componentState);
					return true;
				}
			})
			.forEach(function (dependent) {
				dependent.component.set({});
			});

		this.fire('update', {
			changed: changed,
			previous: previous,
			current: this._state
		});
	},

	_sortComputedProperties: function _sortComputedProperties() {
		var computed = this._computed;
		var sorted = this._sortedComputedProperties = [];
		var visited = blankObject();
		var currentKey;

		function visit(key) {
			var c = computed[key];

			if (c) {
				c.deps.forEach(function (dep) {
					if (dep === currentKey) {
						throw new Error(("Cyclical dependency detected between " + dep + " <-> " + key));
					}

					visit(dep);
				});

				if (!visited[key]) {
					visited[key] = true;
					sorted.push(c);
				}
			}
		}

		for (var key in this._computed) {
			visit(currentKey = key);
		}
	},

	compute: function compute(key, deps, fn) {
		var this$1 = this;

		var value;

		var c = {
			deps: deps,
			update: function (state, changed, dirty) {
				var values = deps.map(function (dep) {
					if (dep in changed) { dirty = true; }
					return state[dep];
				});

				if (dirty) {
					var newValue = fn.apply(null, values);
					if (this$1._differs(newValue, value)) {
						value = newValue;
						changed[key] = true;
						state[key] = value;
					}
				}
			}
		};

		this._computed[key] = c;
		this._sortComputedProperties();

		var state = assign({}, this._state);
		var changed = {};
		c.update(state, changed, true);
		this._set(state, changed);
	},

	fire: fire,

	get: get,

	on: on,

	set: function set$$1(newState) {
		var oldState = this._state;
		var changed = this._changed = {};
		var dirty = false;

		for (var key in newState) {
			if (this._computed[key]) { throw new Error(("'" + key + "' is a read-only computed property")); }
			if (this._differs(newState[key], oldState[key])) { changed[key] = dirty = true; }
		}
		if (!dirty) { return; }

		this._set(newState, changed);
	}
});

var HiveID = function HiveID(options) {
  if (!options.api_id) {
    throw new Error('You need add block api_id');
  }

  if (!options.api_id.partner_id) {
    throw new Error('You need add api_id.app_id');
  }

  if (!options.api_id.app_id) {
    throw new Error('You need add api_id.app_id');
  }

  this.target = options.target;
  this.apiId = options.api_id;
  this.app = null;
  this.callback = options.callback;

  this.destroy = this.destroy.bind(this);
  this.init = this.init.bind(this);
};

HiveID.prototype.destroy = function destroy () {
  this.app && this.app.destroy();
};

HiveID.prototype.init = function init () {
  this.app = new Modal({
    target: this.target || document.body,
    store: new Store({
      apiId: this.apiId,
      closeModal: this.destroy,
      callback: this.callback
    }),
  });
};

exports.default = HiveID;
//# sourceMappingURL=hiveid-sdk.cjs.js.map
